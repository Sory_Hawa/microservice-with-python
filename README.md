# microservices training

Lab

- Installing Docker and kubernetes with minike
- Installing VS Code Extension on Ubuntu
- Creating Python Virtual Environments
- Installing Python VS Code Extension
- Using Pip to Freeze Python Dependencies

Deploy in Docker && kubernetes

- Building the docker image using Dockerfile
- Writing Docker Compose file
- Writing Kubernetes Manifest files for the application
- Creating Helm Chart
- Using ArgoCD to deploy application in kubernetes
